#coding: utf-8
#author: mau

#for -->
#obj.__itr__()
class CuentaAtras():
    def __init__(self,iniciar):
        self.cantidad = iniciar

    def __iter__(self):
        return self

    def __next__(self):
        if self.cantidad <= 0:
            raise StopIteration
        r = self.cantidad
        self.cantidad -=1
        return r

class Rangos():
    def __init__(self,min,max):
        self.min = min
        self.max = max
    def __iter__(self):
        return self
    def __next__(self):
        if self.min > self.max:
            raise StopIteration
        self.min += 1
        return self.min -1

class Usuarios():
    def __init__(self, *args):
        self.usuarios = args
        self.max = len(self.usuarios)
        self.min = 0

    def __iter__(self):
        return self

    def __next__(self):
        if self.min >= self.max:
            raise StopIteration
        self.min += 1
        return self.usuarios[self.min-1]

class Persona():
    def __init__(self,nombre):
        self.nombre = nombre

    def __str__(self):
        return f"Mi Nombre es: {self.nombre}"

class Doctor(Persona):
    def __init__(self,nombre,officio):
        Persona.__init__(self,nombre)
        self.officio = officio
    def __str__(self):
        return f"{super().__str__()} Mi Officio es: {self.officio}"


for objetos in Usuarios(Persona("Antonio"),Persona("Juan"),Persona("Nicolette"),
                        Doctor("Virgo","Operador")):
    print(objetos)



